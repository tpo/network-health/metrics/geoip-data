#
# The provisioning is parsed from the .gitlab-ci.yml's "build" job.
#
# To run:
#  vagrant up
#  vagrant ssh -c 'ls -l $CI_PROJECT_DIR/public/'

require 'pathname'
require 'tempfile'
require 'yaml'

srvpath = Pathname.new(File.dirname(__FILE__)).realpath
configfile = YAML.load_file(File.join(srvpath, "/.gitlab-ci.yml"))

# set up essential environment variables
env = Hash.new
env['CI_SERVER'] = "yes"
env['CI_SERVER_URL'] = "https://gitlab.torproject.org"
env['CI_BUILDS_DIR'] = "/builds"
if ENV.include? 'CI_PROJECT_PATH'
  env['CI_PROJECT_PATH'] = ENV['CI_PROJECT_PATH']
else
  env['CI_PROJECT_PATH'] = "tpo/network-health/metrics/geoip-data"
end
env['CI_PROJECT_DIR'] = "#{env['CI_BUILDS_DIR']}/#{env['CI_PROJECT_PATH']}"
env_file = Tempfile.new('env')
File.chmod(0644, env_file.path)
env.each do |k,v|
    env_file.write("export #{k}='#{v}'\n")
end
env_file.rewind

sourcepath = '/etc/profile.d/env.sh'
header = "#!/bin/bash -ex\nsource #{sourcepath}\ntest -e $CI_PROJECT_DIR && cd $CI_PROJECT_DIR\n"

script_file = Tempfile.new('script')
File.chmod(0755, script_file.path)
script_file.write(header)
configfile['build']['script'].flatten.each do |line|
    script_file.write(line)
    script_file.write("\n")
end
script_file.rewind

Vagrant.configure("2") do |config|
  config.vm.box = "debian/bullseye64"
  config.vm.synced_folder '.', '/vagrant', disabled: true
  config.vm.provision "file", source: env_file.path, destination: 'env.sh'
  config.vm.provision :shell, inline: <<-SHELL
    set -ex

    sed -i s,http:,https:, /etc/apt/sources.list
    apt-get update
    apt-get -qy dist-upgrade
    apt-get -qy install git

    mv ~vagrant/env.sh #{sourcepath}
    source #{sourcepath}
    mkdir -p $(dirname $CI_PROJECT_DIR)
    git clone #{env['CI_SERVER_URL']}/#{env['CI_PROJECT_PATH']} $CI_PROJECT_DIR
    chown -R vagrant.vagrant $(dirname $CI_PROJECT_DIR)
SHELL
  config.vm.provision "file", source: script_file.path, destination: 'script.sh'
  config.vm.provision :shell, inline: '/home/vagrant/script.sh'

  config.vm.provider :libvirt do |libvirt|
    libvirt.memory = 1536
  end
end
